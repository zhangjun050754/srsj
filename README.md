# Simple Rtmp Server-Java

#### Description
基于netty的java版rtmp服务器

#### Software Architecture
jdk1.8，netty4.1.34.Final

#### Installation

1. mvn -DskipTests=true package（clone后用maven打成jar包）
2. java -jar srsj-0.3-SNAPSHOT.jar （直接启动 默认端口 springboot:8080 rtmp:1935 http-flv:8762）
3. java -DSERVER_PORT=8081 -DRTMP_PORT=1936 -DHTTP_PORT=8763 -jar srsj-0.3-SNAPSHOT.jar （指定端口号启动，也可以用export SERVER_PORT=8081指定端口）
4. 使用obs推流，rtmp://127.0.0.1:1935/myapp/stream（myapp和stream可以自定义，但必填）
5. 使用vlc或者flv.js播放，http://127.0.0.1:8762/myapp/stream.flv

#### Instructions

推流仅使用obs测试通过，推流格式仅H.264测试通过，播放仅使用vlc和flv.js测试通过。

不支持rtmp的复杂握手，不支持加密推流拉流，仅支持http-flv播放，不支持hls。（所有不支持的，都在计划开发中。。。菜的真实 :sweet: ）

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)